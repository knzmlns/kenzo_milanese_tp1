# A Prérequis
* Installation centos Configuration de l'os 
* Partitionement avancé avec LVM
* pvcreate /dev/sdb
* vgcreate data /dev/sdb
* lvcreate -L 2048M data -n SRV
* lvcreate -L 3056M data -n HOME
* mkfs -t ext4 /dev/data/SRV
* mkfs -t ext4 /dev/data/HOME
* Création des points de montage pour les serveurs
* mkdir /srv/data1
* mkdir /srv/data2
* mount /dev/data/SRV /srv/data1
* mount /dev/data/HOME /srv/data2
* creation user et ajout mot de passe sudo 
* check en faisant lsblk 
* check montage partitions 
* cat /etc/fstab 
* configuration serveur ssh putty
* ssh-keygen
* sudo systemctl enable sshd
* sudo vim /etc/ssh/sshd_config
* sudo systemctl restart sshd

# B Réseau & ssh & firewall 
# 1)Réseau
*    Node1) 
*    Virtualbox\>accès par pont + réseau privé hôte 
* edition du fichier hostname nom = node1.tp1.b2
* edition fichier /etc/sysconfig/network-scripts/(périphérique)
* Ip statique
*   192.168.1.11 
*    Passerelle par défaut 192.168.1.1 
*    64 bytes from 192.168.1.12: icmp\_seq=4 ttl=64 time=0.323 ms

*    Node2)
   * Virtualbox>accès par pont + réseau privé hôte
   * edition du fichier hostname
   * nom = node2.tp1.b2
*    edition fichier /etc/sysconfig/network-scripts/(périphérique)
   *     - Ip statique
   *     - 192.168.1.12
        Passerelle par défaut 192.168.1.1
   * 64 bytes from 192.168.1.11: icmp_seq=4 ttl=64 time=0.236 ms


# 2)Firewall
* firewall-cmd --zone=public --permanent --add-service=http
*   firewall-cmd --zone=public --permanent --add-service=https
*    firewall-cmd --zone=public --permanent --add-service=ssh
*    firewall-cmd --reload

# C Install serv web

*    installation nginx
*    creation groupe d'user nginx
*    sudo groupadd nginx
*    creation user ng qui appartient au groupe nginx
*    chmod groupe nginx pour les sites
 *   modification /etc/nginx/nginx.conf pour ajouter le paths des sites

```
        user ng;
        server {
        listen 80;
        server_name node1.tp1.b2;
        
        location / {
        return 301 /site1;
        }
        location /site1 {
            alias/srv/site1
        }
        location /site2 {
        alias /srv/site2;
        }
    }
```

 *   Lien entre les sites
 *   curl -kL http://node1.tp1.b2/site1
 *   1
*    curl -kL http://node1.tp1.b2/site2
 *   2

# D HTTPS

*    Génération clé + certificat
*    openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt
*    déplacement des nouveaux fichiers dans /etc/pki/tls/certs (certificat) et /etc/pki/tls/private (clé)
*    config nginx
*    server {
```
       listen 443;

        server_name node1.tp1.b2;
        ssl_certificate /etc/pki/tls/certs/node1.tp1.b2.crt;
        ssl_certificate_key /etc/pki/tls/private/node1.tp1.b2.key;

        location / {
            return 301 /site1;
        }

        location /site1 {
            alias /srv/site1;
        }
        location /site2 {
            alias /srv/site2;
        }
    }

}
```


# E script sauvegarde 
*   Creation script /srv/tp1\_backup.sh 
*   chmod script
```
    find ./ -mtime +7 -type f -delete (supprimer le fichier le moins récent si nb fichier > 7)
    tar -zcvf "site_$(date '+%Y-%m-%d-%I-%M').tar.gz" site1/ site2/ (compression de l'archive)
```


# F Crontab 
* Utilisation de crontab par l'user ng pour sauvegarder les sites
* crontab -u ng -e 
* configuration du crontab 0 */1 * \* \ /srv/tp1\_backup.sh

# G Restauration

 *   Extraire l'archive sauvegardée à la racine préalablement(/srv)
 *   tar -xf archive.tar.gz

# H Netdata install

*    Installation pré-requis
*    sudo yum install zlib-devel libuuid-devel libmnl-devel gcc make git autoconf autogen automake pkgconfig
*    sudo yum install curl jq nodejs
*    Installation direct de Netdata depuis la source Github
*    bash <(curl -Ss https://my-netdata.io/kickstart.sh)

*    Paramétrage firewall    
*   sudo firewall-cmd --permanent --zone=public --add-port=19999/tcp
*    sudo firewall-cmd --reload

# I Netdata Alert

*    Création d'un serveur + webhook discord
*   aller dans la config de net data et coller sa clé sur cette ligne
*    DISCORD_WEBHOOK_URL;
*    Reset Nginx
*    modification config nginx
```
     upstream netdata {
       server 127.0.0.1:19999;
       keepalive 64;
    }

    server {
       listen 80;

     # the virtual host name of this subfolder should be exposed
     #server_name netdata.example.com;

     location = /netdata {
            return 301 /netdata/;
     }

    location ~ /netdata/(?<ndpath>.*) {
            proxy_redirect off;
            proxy_set_header Host $host;

            proxy_set_header X-Forwarded-Host $host;
            proxy_set_header X-Forwarded-Server $host;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_http_version 1.1;
            proxy_pass_request_headers on;
            proxy_set_header Connection "keep-alive";
            proxy_store off;
            proxy_pass http://netdata/$ndpath$is_args$args;

            gzip on;
            gzip_proxied any;
            gzip_types *;
        }

}
```

